package io;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Points {

	private final Map<String, Object> properties = new HashMap<>();

	public Collection<String> propertyNames() {
		return new ArrayList<>(properties.keySet());
	}

	public Object getProperty(final String propName) {
		return properties.get(propName);
	}

	public void setProperty(final String propName, final Object propValue) {
		if (propValue == null) {
			properties.remove(propName);
		} else {
			properties.put(propName, propValue);
		}
	}

	@Override
	public String toString() {
		return properties + " + " + points;
	}

	private final List<Point> points = new ArrayList<>();

	public int getPointCount() {
		return points.size();
	}

	public Point getPoint(final int pos) {
		return points.get(pos);
	}

	public void addPoint(final Point p) {
		points.add(p);
	}

	public void removePoint(final Point p) {
		points.remove(p);
	}

	public void addPoint(final int pos, final Point p) {
		points.add(pos, p);
	}

	public void setPoint(final int pos, final Point p) {
		points.set(pos, p);
	}
}
