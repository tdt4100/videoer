package io;

public class Point {

	private final double x;
	private final double y;

	public Point(final double x, final double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point[x=" + x + " y=" + y + "]";
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public static Point valueOf(final String s) {
		final int pos = s.indexOf(',');
		final double x = Double.valueOf(s.substring(0, pos));
		final double y = Double.valueOf(s.substring(pos + 1));
		return new Point(x, y);
	}
}
