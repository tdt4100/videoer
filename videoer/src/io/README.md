# Input/output

I dette eksemplet ser vi på input/output av en samling ([Points](Points.java)) av punkter ([Point](Point.java)) vha. noen av Java sine klasser for input/output (io).
Vi har også laget en JavaFX-app som illustrerer bruken av IO i en relevant sammenheng.

## Klassene

- [Point](Point.java) - En enkel dataklasse for et x,y-koordinatpar.
- [Points](Points.java) - En såkalt "container"-klasse som holder en samling av [Point](Point.java)-objekter, samt et sett med nøkkel-verdi-par for meta-data om punktsamlingen.
- [PointsIO](PointsIO.java) - Klassen som implenterer lesing og skriving av et enkelt tekstformat for punktsamlingen m/metadata.
- [PointsApp](PointsApp.java) - Applikasjonsklassen, som leser inn og viser frem innholdet i [Points.fxml](Points.fxml).
- [Points.fxml](Points.fxml) - Grensesnittet består av et tekstfelt for filnavn eller URL, noen aksjonsknapper og et visningspanel for punktene som en strek-graf.
- [PointsController](PointsController.java) - Reagerer på aksjonsknappene (og tekstfeltet) ved å lese inn punkter og viser dem frem.

## Kjøre appen

Kjør appen med `mvn javafx:run -Djavafx.mainClass=io.PointsApp` i terminal-panelet.
Du kan også velge `Terminal > Run Task... > Run io.PointsApp`

## Tekstformatet

Formatet er linjebasert og støtter

- kommentarer fra og med #-tegnet
- egenskaper med navn og verdi (kjenner igjen true/false (Boolean), heltall (Integer) og tar resten som String)
- punktene, som x,y-par skilt med mellomrom (whitespace)

Eksempel:

```
# everything after the first # is a comment
name: Dataset for measurement  # name property, type defaults to String
seriesNum: 1 # numbers are recognized
x-axis-is-time:true # booleans are recognized

3,4 5,3
6,2
```
