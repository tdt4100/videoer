package io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.net.URL;

public class PointsIO {

	public Points readPoints(final InputStream input) throws IOException {
		return readPoints(new InputStreamReader(input));
	}

	public Points readPoints(final Reader reader) throws IOException {
		final Points points = new Points();
		final BufferedReader bufferedReader = new BufferedReader(reader);
		String line = null;
		int lineNum = 0;
		while ((line = bufferedReader.readLine()) != null) {
			final int hashPos = line.indexOf('#');
			if (hashPos >= 0) {
				line = line.substring(0, hashPos);
			}
			line = line.trim();
			lineNum++;
			if (line.length() == 0) {
				continue;
			}
			try {
				final int colonPos = line.indexOf(':');
				if (colonPos >= 0) {
					// propertyName:propertyValue
					final String propName = line.substring(0, colonPos).trim();
					final String propValue = line.substring(colonPos + 1).trim();
					Object propObject = null;
					if (propValue.equalsIgnoreCase("true")) {
						propObject = Boolean.TRUE;
					} else if (propValue.equalsIgnoreCase("false")) {
						propObject = Boolean.FALSE;
					} else {
						try {
							propObject = Integer.valueOf(propValue);
						} catch (final NumberFormatException e) {
							propObject = propValue;
						}
					}
					points.setProperty(propName, propObject);
				} else {
					// space separated coordinate pairs x,y x,y
					// split takes a regex or Pattern:
					// see https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/regex/Pattern.html
					final String[] pointStrings = line.split("\\s+");
					for (final String pointString : pointStrings) {
						points.addPoint(Point.valueOf(pointString));
					}
				}
			} catch (final RuntimeException e) {
				throw new RuntimeException("Problem when parsing line " + lineNum + ": " + e.getMessage(), e);
			}
		}
		return points;
	}

	public void writePoints(final Points points, final OutputStream outputStream) {
		final PrintStream printStream = new PrintStream(outputStream);
		printStream.println("# properties");
		for (final String propName : points.propertyNames()) {
			final Object value = points.getProperty(propName);
			printStream.println(propName + ":" + value);
		}
		printStream.println("# points");
		for (int i = 0; i < points.getPointCount(); i++) {
			final Point point = points.getPoint(i);
			if (i > 0) {
				printStream.print(" ");
			}
			printStream.print(point.getX() + "," + point.getY());
		}
	}

	public static void main(final String[] args) {
		final PointsIO io = new PointsIO();
		try (InputStream inputStream = PointsIO.class.getResourceAsStream("sample-points.txt")) {
			final Points points = io.readPoints(inputStream);
			System.out.println(points);
		} catch (final IOException e) {
			System.err.println(e);
		}
		try (InputStream inputStream = new URL("https://gitlab.stud.idi.ntnu.no/tdt4100/videoer/-/raw/master/videoer/src/io/sample-points3.txt").openStream()) {
			final Points points = io.readPoints(inputStream);
			System.out.println(points);
		} catch (final IOException e) {
			System.err.println(e);
		}
		//		try (Reader inputStream = new FileReader("/Users/hal/java/git/tdt4100-gitlab/videoer/videoer/src/io/sample-points2.txt")) {
		//			final Points points = io.readPoints(inputStream);
		//			System.out.println(points);
		//			try (OutputStream outputStream = new FileOutputStream("/Users/hal/java/git/tdt4100-gitlab/videoer/videoer/src/io/sample-points3.txt")) {
		//				io.writePoints(points, outputStream);
		//			}
		//		} catch (final IOException e) {
		//			System.err.println(e);
		//		}
	}
}

/*
Example format:

# everything after the first # is a comment
name: Dataset for measurement  # name property, type defaults to String
seriesNum: 1 # numbers are recognized
x-axis-is-time:true # booleans are recognized

3,4 5,3
6,2
 */
