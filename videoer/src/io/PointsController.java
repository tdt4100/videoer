package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

public class PointsController {

	private Points points;

	private PointsIO io;

	@FXML
	private void initialize() {
		io = new PointsIO();
		try {
			points = io.readPoints(PointsApp.class.getResourceAsStream("sample-points.txt"));
		} catch (final IOException e) {
		}
		lineChart = new LineChart<Number, Number>(new NumberAxis(), new NumberAxis());
		lineChartPane.getChildren().add(lineChart);
		updateLineChart();
	}

	@FXML
	private TextField inputSourceText;

	@FXML
	private Pane lineChartPane;

	private FileChooser fileChooser = null;
	private Alert alert = null;

	@FXML
	private void openAction() {
		final String inputSource = inputSourceText.getText();
		if (inputSource.length() == 0) {
			return;
		}
		points = null;
		try {
			if (inputSource.indexOf(':') > 3) {
				points = io.readPoints(new URL(inputSource).openStream());
			} else {
				points = io.readPoints(new FileInputStream(inputSource));
			}
			final String lastSegment = inputSource.substring(inputSource.lastIndexOf('/') + 1);
			if (points.getProperty("chart-title") == null) {
				lineChart.setTitle(lastSegment);
			}
		} catch (final Exception e) {
			if (alert == null) {
				alert = new Alert(AlertType.ERROR, e.getMessage());
			}
			alert.show();
		}
		updateLineChart();
	}

	@FXML
	private void browseAction() {
		if (fileChooser == null) {
			fileChooser = new FileChooser();
		}
		final File file = fileChooser.showOpenDialog(inputSourceText.getScene().getWindow());
		inputSourceText.setText(file.getAbsolutePath().toString());
		openAction();
	}

	private LineChart<Number, Number> lineChart;

	private void updateLineChart() {
		lineChart.getData().clear();
		if (points != null) {
			updateLineChartMetaData();
			updateLineChartData();
		}
	}

	private void updateLineChartData() {
		final XYChart.Series<Number, Number> series = new XYChart.Series<>();
		for (int i = 0; i < points.getPointCount(); i++) {
			final Point p = points.getPoint(i);
			series.getData().add(new XYChart.Data<Number, Number>(p.getX(), p.getY()));
		}
		final Object seriesName = points.getProperty("series-name");
		if (seriesName != null) {
			series.setName(String.valueOf(seriesName));
		}
		lineChart.getData().add(series);
	}

	private void updateLineChartMetaData() {
		final Object chartTitle = points.getProperty("chart-title");
		if (chartTitle != null) {
			lineChart.setTitle(String.valueOf(chartTitle));
		}
		final Object xAxisLabel = points.getProperty("x-axis-label");
		if (xAxisLabel != null) {
			lineChart.getXAxis().setLabel(String.valueOf(xAxisLabel));
		}
		final Object yAxisLabel = points.getProperty("y-axis-label");
		if (yAxisLabel != null) {
			lineChart.getYAxis().setLabel(String.valueOf(yAxisLabel));
		}
	}
}
