package arv2;

/*
 * @startuml
 * class Object {
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * class Point2d extends Object {
 * 	double x
 * 	double y
 * 	Point2d(double x, double y)
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * class Point3d extends Point2d{
 * 	double z
 * 	Point3d(double x, double y, double z)
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * @enduml
 */
public class Point3d extends Point2d {

	private double z;

	public Point3d(double x, double y, double z) {
		super(x, y);
		this.z = z;
	}

	@Override
	public String toString() {
		return "Point3d[" + toStringHelper() + "]";
	}

	@Override
	protected String toStringHelper() {
		return super.toStringHelper() + " z=" + z;
	}

	@Override
	public boolean equals(Object o) {
		if (! super.equals(o)) {
			return false;
		}
		if (o.getClass() == this.getClass()) {
			Point3d other = (Point3d) o;
			return this.z == other.z;
		}
		return false;
	}

	public int coordCount() {
		return 3;
	}
	
	public double getCoord(int num) {
		if (num == 2) {
			return z;
		}
		return super.getCoord(num);
	}

	public static void main(String[] args) {
		Point3d p1 = new Point3d(1, 2, 3);
		Point3d p2 = new Point3d(1, 4, 3);
		Point3d p3 = new Point3d(1, 2, 3);
		Point2d p4 = new Point2d(1, 2);
		System.out.println(p1.equals(p2));
		System.out.println(p1.equals(p3));
		System.out.println(p3.equals(p4));
		System.out.println(p4.equals(p3));
	}
}
