package arv2;

/*
 * @startuml
 * abstract class AbstractPoint {
 * 	{abstract} int coordCount()
 * 	{abstract} double getCoord(int num)
 * 
 * 	double distance()
 * 	double dot(AbstractPoint other)
 * }
 * class Point2d extends AbstractPoint {
 * 	int coordCount()
 * 	double getCoord(int num)
 * }
 * class Point3d extends Point2d {
 * 	int coordCount()
 * 	double getCoord(int num)
 * }
 * class PointNd extends AbstractPoint {
 * 	int coordCount()
 * 	double getCoord(int num)
 * }
 * @enduml
 */

public abstract class AbstractPoint {

	public abstract int coordCount();
	public abstract double getCoord(int num);
	
	private double distance = -1;
	
	public double distance() {
		if (distance < 0) {
			this.distance = computeDistance();
		}
		return distance;
	}

	private double computeDistance() {
		double sqSum = 0.0;
		for (int coordNum = 0; coordNum < coordCount(); coordNum++) {
			double coord = getCoord(coordNum);
			sqSum += coord * coord;
		}
		return Math.sqrt(sqSum);
	}
	
	public double dot(AbstractPoint other) {
		double dot = 0.0;
		for (int coordNum = 0; coordNum < coordCount(); coordNum++) {
			double coord1 = getCoord(coordNum);
			double coord2 = other.getCoord(coordNum);
			dot += coord1 * coord2;
		}
		return dot;
	}
}
