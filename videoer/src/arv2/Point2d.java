package arv2;

/*
 * @startuml
 * class Object {
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * class Point2d extends Object {
 * 	double x
 * 	double y
 * 	Point2d(double, double)
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * @enduml
 */
public class Point2d extends AbstractPoint {

	protected double x;
	protected double y;

	public Point2d(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point2d[" + toStringHelper() + "]";
	}

	protected String toStringHelper() {
		return "x=" + x + " y=" + y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		// hvis o er et Point2d og ikke en instans av en subklasse med samme x, y som this,
		// så returneres true
		if (o.getClass() == this.getClass()) {
			Point2d other = (Point2d) o;
			return this.x == other.x && this.y == other.y;
		}
		return false;
	}
	
	@Override
	public int coordCount() {
		return 2;
	}
	
	@Override
	public double getCoord(int num) {
		if (num == 0) {
			return x;
		} else if (num == 1) {
			return y;
		} else {
			throw new IllegalArgumentException(num + " must be lower than 2");
		}
	}

	public static void main(String[] args) {
		Point2d p1 = new Point2d(1, 2);
		double distance = p1.distance();
		System.out.println(distance);
	}
}
