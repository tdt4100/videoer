module videoer {
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.controls;

	exports observerbar3;
	exports io;

	opens observerbar3 to javafx.fxml;
	opens io to javafx.fxml;
}