package observerbar3;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class EatController implements CharGridListener {

	// (unicode) characters used for player and for 'pills' to eat

	private final static char playerChar = '\u2654';
	private final static char[] pillChars = { '\u265A', '\u265B', '\u265C', '\u265D', '\u265E', '\u265F' };

	// game state variables

	private CharGrid grid;

	private int row, column;
	private long lastScoreTime;
	private int score = 0;

	// UI elements

	@FXML
	private Label scoreLabel;
	private final String scoreLabelFormat = "Score: %d";

	@FXML
	private GridPane gridPane;

	@FXML
	private void initialize() {
		startGame(5);
	}

	private void startGame(final int gridSize) {
		// initialize grid
		grid = new CharGrid(gridSize, ' ');
		// player position
		this.row = gridSize / 2;
		this.column = gridSize / 2;
		grid.setChar(row, column, playerChar);
		// place pills in random positions
		int numPillsLeft = gridSize;
		while (numPillsLeft > 0) {
			final int row = (int)(Math.random() * gridSize);
			final int column = (int)(Math.random() * gridSize);
			if (grid.getChar(row, column) == grid.getDefaultChar()) {
				grid.setChar(row, column, pillChars[(int) (Math.random() * pillChars.length)]);
				numPillsLeft--;
			}
		}
		final int tileSize = 40;
		// clear and refill the grid with Label objects
		gridPane.getChildren().clear();
		for (int row = 0; row < gridSize; row++) {
			for (int column = 0; column < gridSize; column++) {
				final Label label = new Label();
				label.setPrefWidth(tileSize);
				label.setPrefHeight(tileSize);
				gridPane.getChildren().add(label);
				GridPane.setRowIndex(label, row);
				GridPane.setColumnIndex(label, column);
				// ensure label is updated with grid value
				updateLabel(row, column);
			}
		}
		lastScoreTime = System.currentTimeMillis();
		updateScoreLabel();
		// remember to register ourselves as grid change listener
		grid.addCharGridListener(this);
	}

	private void updateScoreLabel() {
		scoreLabel.setText(String.format(scoreLabelFormat, score));
	}

	// update label with corresponding grid value
	private void updateLabel(final int row, final int column) {
		// find Label object corresponding to grid cell at row, column
		final Label label = (Label) gridPane.getChildren().get(row * grid.getSize() + column);
		// set to corresponding String
		label.setText(getLabelForCell(grid.getChar(row, column)));
	}

	// compute label text for char grid value
	// easier to change later, when it's done in a separate method
	private String getLabelForCell(final char c) {
		return String.valueOf(c);
	}

	// called for each key press
	// registered on the Scene in the FXML
	@FXML
	private void handleKeyPressed(final KeyEvent keyEvent) {
		// key constants are defined in javafx.scene.input.KeyCode enum
		switch (keyEvent.getCode()) {
		case LEFT:	moveChar( 0, -1); break;
		case RIGHT: moveChar( 0,  1); break;
		case UP: 	moveChar(-1,  0); break;
		case DOWN:	moveChar( 1,  0); break;
		default:
			break;
		}
	}

	// called for each arrow key
	private void moveChar(final int dr, final int dc) {
		// compute new row, column values for player
		final int newRow = row + dr, newColumn = column + dc;
		final int size = grid.getSize();
		// is the new position within the grid?
		if (newRow >= 0 && newRow < size && newColumn >= 0 && newColumn < size) {
			final char oldChar = grid.moveChar(row, column, dr, dc);
			// did we eat a pill/piece?
			if (oldChar != grid.getDefaultChar()) {
				final int pillScore = getScoreForPill(oldChar) * 10;
				final long scoreTime = System.currentTimeMillis();
				final long second = (scoreTime - lastScoreTime) / 1000;
				score += Math.pow(pillScore, 1.0 / (second + 1));
				lastScoreTime = scoreTime;
				updateScoreLabel();
			}
			// we don't need these, since we are listening for grid changes
			//			updateLabel(row, column);
			//			updateLabel(newRow, newColumn);
			row = newRow;
			column = newColumn;
			if (grid.count(c -> c == grid.getDefaultChar()) == size * size - 1) {
				startGame(size + 1);
			}
		}
	}

	// helper method, score will depend on position in pillChars, earlier means higher score
	private int getScoreForPill(final char c) {
		for (int i = 0; i < pillChars.length; i++) {
			if (pillChars[i] == c) {
				return pillChars.length - i;
			}
		}
		return 0;
	}

	// called whenever the grid changes
	@Override
	public void gridChanged(final GridChangeEvent event) {
		// System.out.println was used in the video for demo purposes
		//		System.out.println("Rute endret: " + event.row + "," + event.column);
		// make sure JavaFX Label is up-to-date with the underlying CharGrid value
		updateLabel(event.row, event.column);
	}
}
