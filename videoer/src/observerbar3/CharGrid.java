package observerbar3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

public class CharGrid {

	private final char defaultChar;
	private final char[][] chars;

	/**
	 * Initializes the grid with the given size, and fills it with the given defaultChar.
	 * @param size
	 * @param defaultChar
	 */
	public CharGrid(final int size, final char defaultChar) {
		// create two-dimensional grid of size width and height
		chars = new char[size][];
		for (int i = 0; i < chars.length; i++) {
			chars[i] = new char[size];
			for (int j = 0; j < chars.length; j++) {
				chars[i][j] = defaultChar;
			}
		}
		this.defaultChar = defaultChar;
	}

	/**
	 * Gets the grid's size (width and height)
	 * @return the size (width and height)
	 */
	public int getSize() {
		return chars.length;
	}

	/**
	 * Gets the default character, i.e. the one used for empty cells.
	 * @return the default character
	 */
	public char getDefaultChar() {
		return defaultChar;
	}

	/**
	 * Gets the char at the given row and column
	 * @param row
	 * @param column
	 * @return the char at the given row and column
	 */
	public char getChar(final int row, final int column) {
		return chars[row][column];
	}

	/**
	 * Counts the number of grid characters satisfying the given predicate.
	 * Useful for checking it a level has finished.
	 * @param what the predicate to test
	 * @return the number of grid characters satisfying the given predicate
	 */
	public int count(final Predicate<Character> what) {
		int count = 0;
		for (int row = 0; row < getSize(); row++) {
			for (int column = 0; column < getSize(); column++) {
				final char c = getChar(row, column);
				if (what.test(c)) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * Sets the char at the given row and column.
	 * @param row
	 * @param column
	 * @param newChar
	 * @return the previous char at the given row and column
	 */
	public char setChar(final int row, final int column, final char newChar) {
		final char oldChar = chars[row][column];
		chars[row][column] = newChar;
		fireCharGridChanged(row, column);
		return oldChar;
	}

	/**
	 * Move the char at the given row and column to row+dr and column+dc.
	 * Typically used as response to pressing arrow keys.
	 * @param row
	 * @param column
	 * @param dr
	 * @param dc
	 * @return
	 */
	public char moveChar(final int row, final int column, final int dr, final int dc) {
		final char oldChar = getChar(row + dr, column + dc);
		setChar(row + dr, column + dc, getChar(row, column));
		setChar(row, column, defaultChar);
		return oldChar;
	}

	// listener handling

	private final Collection<CharGridListener> listeners = new ArrayList<>();

	protected void fireCharGridChanged(final int row, final int column) {
		final GridChangeEvent event = new GridChangeEvent(this, row, column);
		for (final CharGridListener listener : listeners) {
			listener.gridChanged(event);
		}
	}

	/**
	 * Adds the given listener to the list of listeners that will be notified when the grid changes.
	 * @param listener
	 */
	public void addCharGridListener(final CharGridListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the given listener from the list of listeners that will be notified when the grid changes.
	 * @param listener
	 */
	public void removeCharGridListener(final CharGridListener listener) {
		listeners.remove(listener);
	}
}
