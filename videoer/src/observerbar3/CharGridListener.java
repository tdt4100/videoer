package observerbar3;

public interface CharGridListener {

	/**
	 * Called when a CharGrid is changed.
	 * @param event details about the change
	 */
	public void gridChanged(GridChangeEvent event);
}
