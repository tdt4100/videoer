package observerbar3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class EatApp extends Application {

	@Override
	public void start(final Stage primaryStage) throws Exception {
		// Note that the root element is a Scene, not a Parent or Pane
		final Scene scene = FXMLLoader.load(getClass().getResource("Eat.fxml"));
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(final String[] args) {
		launch(args);
	}
}
