package observerbar3;

public class GridChangeEvent {

	CharGrid grid;
	int row;
	int column;

	public GridChangeEvent(final CharGrid grid, final int row, final int column) {
		super();
		this.grid = grid;
		this.row = row;
		this.column = column;
	}
}
