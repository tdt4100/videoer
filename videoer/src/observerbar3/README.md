# Observatør-observert-teknikken 3

Dette eksemplet illustrerer ulike valgmuligher ved bruk av observatør-observert-teknikken.

Eksempel-appen er et enkelt spill, hvor en (sjakk)figur går rundt i et rutenett og "spiser" det som er i andre ruter (også sjakkbrikker). Vi ønsker å skille mellom den "interne" representasjonen av rutenettet og "fasaden" som brukeren ser, og bruke _observatør-observert-teknikken_ for å holde fasaden i synk med den interne representasjonen.

## Kjøre appen

Kjør appen med `mvn javafx:run -Djavafx.mainClass=observerbar3.EatApp` i terminal-panelet.
Du kan også velge `Terminal > Run Task... > Run observerbar3.EatApp`

## Observerbarhet og konstruksjonsvalg

Rutenettet er representert ved en todimensjonal char-tabell (`char[][]`) som er kapslet inn av `CharGrid`-klassen, som bl.a. har metoder for å hente ut og endre char-verdien i en rute. For å kunne bruke observatør-observert-teknikken må `CharGrid` gjøres _observerbar_:

1. Andre objekter må kunne lese ut all nødvendig tilstand fra `CharGrid`-objekter. Dette håndteres med vanlig innkapsling, som jo handler om tilgang til intern tilstand.
2. Andre objekter må kunne (be om å) bli varslet om vesentlige endringer i tilstanden, så de kan reagere på dem og oppdatere annen tilstand, f.eks. JavaFX-objekter i brukergrensesnittet.

Iht. den generelle teknikken, så trengs følgende elementer:

- et lyttergrensesnitt, her har vi laget et `CharGridListener`-grensesnitt
- metoder for å registrere lyttere, her har vi lagt til `addCharGridListener` og `removeCharGridListener` i `CharGrid`
- en (eller flere) hjelpemetode(r) som varsler registrerte lyttere om at tilstanden er endret, her har vi lagt til `fireCharGridChanged`-metoden

Valgene er knyttet til utformingen av grensesnittet:

- Er det behov for flere metoder, fordi det er flere ulike måter å endre rutenettet på?
- Hvordan skal hver metode se ut, mer spesifikt gjennom hva slags argument(er) skal informasjon om endringen formidles?

## Én-metodegrensesnitt og endringshendelsesobjekt

I vår løsning har vi valgt å ha én metode tilsvarende `setChar`, og la `moveChar` varsle to ganger, én for hver rute den endrer. Varslingsgrensesnitt har da naturlig nok bare én metode. Informasjonen om endringen har vi valgt å putte i et _endringshendelse_-objekt av typen `GridChangeEvent`, i stedet for å ha flere spre informasjonen over flere argumenter.

Disse valgene har flere fordeler, for det første kan et grensesnitt med én metode implementeres med en lambda, og for det andre er et slikt hendelsesobjekt en mer "fremtidsrettet" løsning. Hvis en senere ønsker å gi mer informasjon om endringen, f.eks. gammel og ny ruteverdi, så trenger vi ikke å endre `gridChanged`-metoden, vi endrer i stedet `GridChangeEvent`-klassen. Det gjør at eksisterende lytterimplementasjoner ikke nødvendigvis må endres.

Alternativt kunne vi hatt lyttermetoder tilsvarende både `setChar` og `moveChar` og hatt parametre for all informasjon vi trenger å formidle. Det ville vært enklere på noen måter, men gjort andre ting mer komplisert. Valget vi endte opp med er typisk for mange rammeverk: én metode og et endringshendelsesobjekt.

Gitt valget vi har gjort, så trenger vi bare én varslingsmetode. Den lager først endringshendelsesobjektet med den aktuelle informasjonen og gir det så som argument i kallet til lyttermetoden til alle lytterne.

## Felles varsling av ulike typer endringer

Det er enklere med lyttergrensesnitt med én metode, og hvis en har flere endringsmetoder, så må en se om det går an å ha en felles varslingsmetode. Det finnes flere teknikker for det, som vi skal se.

Når en bruker et endringshendelseobjekt, så kan en løse problemet ved å ha ulike typer endringshendelseobjekt tilpasset den faktiske endringer, f.eks. ved å ha én (sub)klasse for hver type endring. Oversatt til vårt tilfelle, så kunne vi hatt klassene `SetCharChangeEvent` og `MoveCharChangeEvent`som subklasser av en felles `GridChangeEvent`-superklasse.

En annen aktuell mulighet er å varsle om mer komplekse endringer i flere trinn. Dette går av seg selv i vår nåværende implementasjon, siden `moveChar` kaller `setChar` to ganger og dermed vil gi to varslinger.

En tredje mulighet er en slags akkumulert varsling, hvor flere endringer slås sammen i ett varsel. Her kan vi tenke oss to varianter. Den ene har en liste med koordinatene til rutene som er endret. For `setChar` vil lista ha ett element og for `moveChar` vil lista ha to. I den andre varianten kombinerer vi koordinatene i et omsluttende rektangel. Så i stedet for å angi de spesifikke koordinatene, så lager vi et rektangel som er stort nok til å omslutte alle rutene som er endret. For `setChar` vil rektanglet inneholde én rute og for `moveChar` vil rektanglet ha to. Dette vil gi mottakeren av varslet mindre presis informasjon, men kan likevel gi mulighet til mer effektiv kode, siden endringene kan håndteres samlet.

En fjerde variant bygger på den tredje, men samler opp endringer fra flere kall til endringsmetodene. Hvis flere endringsmetoder kalles i rask rekkefølge, f.eks. trigget av ett knappetrykk, så slås endringsinformasjonen sammen i ett varsel, som sendes avgårde til slutt. Dette er ikke så aktuelt i vårt eksempel, men er relevant i web-applikasjoner, siden det reduserer antall nettverksforespørsler.
