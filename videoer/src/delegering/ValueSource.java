package delegering;

import java.io.InputStream;
import java.util.Scanner;

public class ValueSource {

	public ValueSource(InputStream input) {
		scanner = new Scanner(input);
	}

	private Scanner scanner;

	public boolean hasNextValue() {
		return scanner.hasNextDouble();
	}

	public double nextValue() {
		double value = scanner.nextDouble();
		return value;
	}
}
