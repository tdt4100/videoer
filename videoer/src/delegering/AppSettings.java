package delegering;

public class AppSettings {

	private Settings defaultSettings = new Settings();
	private Settings userSettings = new Settings();

	public AppSettings() {
		defaultSettings.updateSetting("theme", "light");
		defaultSettings.updateSetting("fontName", "comic sans");
	}

	public boolean hasSetting(final String settingName) {
		return userSettings.hasSetting(settingName) || defaultSettings.hasSetting(settingName);
	}

	public Object getSetting(final String settingName) {
		if (userSettings.hasSetting(settingName)) {
			return userSettings.getSetting(settingName);
		}
		return defaultSettings.getSetting(settingName);
	}

	public void updateSetting(final String settingName, final Object value) {
		userSettings.updateSetting(settingName, value);
	}

	public static void main(String[] args) {
		AppSettings appSettings = new AppSettings();
		System.out.println(appSettings.getSetting("theme"));
		System.out.println(appSettings.getSetting("fontName"));
		appSettings.updateSetting("theme", "dark");
		System.out.println(appSettings.getSetting("theme"));
		System.out.println(appSettings.getSetting("fontName"));
	}

	/*
	 * @startuml
	 * main -> AppSettings: getSetting("theme")
	 * AppSettings -> "userSettings: Settings" as userSettings: hasSetting("theme")
	 * userSettings --> AppSettings: false
	 * AppSettings -> "defaultSettings: Settings" as defaults: getSetting("theme")
	 * defaults --> AppSettings: "light"
	 * AppSettings --> main: "light"
	 *
	 * main -> AppSettings: updateSetting("theme", "dark")
	 * AppSettings -> userSettings: updateSetting("theme", "dark")
	 *
	 * main -> AppSettings: getSetting("theme")
	 * AppSettings -> userSettings: hasSetting("theme")
	 * userSettings --> AppSettings: true
	 * AppSettings -> userSettings: getSetting("theme")
	 * userSettings --> AppSettings: "dark"
	 * AppSettings --> main: "dark"
	 * @enduml
	 */

	/*
	 * @startuml
	 * object "appSettings: DefaultingSettings" as appSettings
	 * object "projectSettings: MapSettings" as project
	 * object "map1: Map" as map1
	 * object "ds2: DefaultingSettings" as ds2
	 * object "workspaceSettings: MapSettings" as workspace
	 * object "map2: Map" as map2
	 * object "installationSettings: MapSettings" as installation
	 * object "map3: Map" as map3
	 * appSettings -down-> project: settings
	 * project -down-> map1: settings
	 * appSettings -down-> ds2: defaultSettings
	 * ds2 -down-> workspace: settings
	 * workspace -down-> map2: settings
	 * ds2 -down-> installation: defaultSettings
	 * installation -down-> map3: settings
	 * @enduml
	 */
}
