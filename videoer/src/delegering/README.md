# Delegering

Denne pakken inneholder to delegeringseksempler.

## ValueAverager-eksemplet

[ValueAverager](ValueAverager.java) utfører en beregning, f.eks. gjennomsnitt, av tall fra en eller annen kilde.
Klassen fungerer som koordinater og delegerer til to hjelpeklasser, en for datakilden og en for beregningen.

## App-innstillinger-eksemplet

I dette eksemplet bruker [AppSettings](AppSettings.java)-klassen delegering til to [Settings](Settings.java)-instanser for å kombinere generelle og brukerspesfikke innstillinger for en (tenkt) app. [Settings](Settings.java)-klassen bruker også delegering, til `Map`-implementasjon `HashMap`.
