package delegering;

public interface ValueComputer {
	public void acceptValue(double value);
	public double computeValue();
}
