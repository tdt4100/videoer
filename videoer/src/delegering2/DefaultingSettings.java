package delegering2;

public class DefaultingSettings implements ISettings {

	private ISettings defaultSettings = new MapSettings();
	private ISettings settings = new MapSettings();

	public DefaultingSettings(ISettings defaultSettings) {
		this.defaultSettings = defaultSettings;
	}

	@Override
	public boolean hasSetting(final String settingName) {
		return settings.hasSetting(settingName) || defaultSettings.hasSetting(settingName);
	}

	@Override
	public Object getSetting(final String settingName) {
		if (settings.hasSetting(settingName)) {
			return settings.getSetting(settingName);
		}
		return defaultSettings.getSetting(settingName);
	}

	@Override
	public void updateSetting(final String settingName, final Object value) {
		settings.updateSetting(settingName, value);
	}

	public static void main(String[] args) {
		ISettings installationSettings = new MapSettings();
		ISettings ds2 = new DefaultingSettings(installationSettings);
		ISettings appSettings = new DefaultingSettings(ds2);
	}

	/*
	 * @startuml
	 * object "appSettings: DefaultingSettings" as appSettings
	 * object "projectSettings: MapSettings" as project
	 * object "map1: Map" as map1
	 * object "ds2: DefaultingSettings" as ds2
	 * object "workspaceSettings: MapSettings" as workspace
	 * object "map2: Map" as map2
	 * object "installationSettings: MapSettings" as installation
	 * object "map3: Map" as map3
	 * appSettings -down-> project: settings
	 * project -down-> map1: settings
	 * appSettings -down-> ds2: defaultSettings
	 * ds2 -down-> workspace: settings
	 * workspace -down-> map2: settings
	 * ds2 -down-> installation: defaultSettings
	 * installation -down-> map3: settings
	 * @enduml
	 */
}
