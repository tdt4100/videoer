package delegering2;

public interface ISettings {

	public boolean hasSetting(final String settingName);

	public Object getSetting(final String settingName);

	public void updateSetting(final String settingName, final Object value);
}
