package observerbar;

public class Beundrer implements Fan {

	private String hårfarge = "lys";
	private int hårlengde = 40;

	public String getHårfarge() {
		return hårfarge;
	}

	public void setHårfarge(String hårfarge) {
		this.hårfarge = hårfarge;
	}

	@Override
	public void idolErEndret(Idol idol, String egenskap) {
		if ("hårfarge".equals(egenskap)) {
			setHårfarge(idol.getHårfarge());
		} else if ("hårlengde".equals(egenskap)) {
			setHårlengde(idol.getHårlengde());
		}
	}

	public int getHårlengde() {
		return hårlengde;
	}

	public void setHårlengde(int hårlengde) {
		this.hårlengde = hårlengde;
	}

	public static void main(String[] args) {
		Idol idol = new Idol();
		Beundrer beundrer = new Beundrer();
		idol.addFan(beundrer);
		idol.setHårfarge("rød");
		System.out.println(idol.getHårfarge());
		System.out.println(beundrer.getHårfarge());
		idol.setHårlengde(80);
		System.out.println(idol.getHårlengde());
		System.out.println(beundrer.getHårlengde());
	}
}
