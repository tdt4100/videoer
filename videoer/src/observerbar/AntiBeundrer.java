package observerbar;

public class AntiBeundrer implements Fan {

	private String hårfarge = "lys";
	private int hårlengde = 40;

	public String getHårfarge() {
		return hårfarge;
	}

	public void setHårfarge(String hårfarge) {
		this.hårfarge = hårfarge;
	}

	@Override
	public void idolErEndret(Idol idol, String egenskap) {
		if ("hårfarge".equals(egenskap)) {
			setHårfarge("not " + idol.getHårfarge());
		} else if ("hårlengde".equals(egenskap)) {
			setHårlengde(100 - idol.getHårlengde());
		}
	}

	public int getHårlengde() {
		return hårlengde;
	}

	public void setHårlengde(int hårlengde) {
		this.hårlengde = hårlengde;
	}

	public static void main(String[] args) {
		Idol idol = new Idol();
		AntiBeundrer antiBeundrer = new AntiBeundrer();
		Beundrer beundrer = new Beundrer();
		idol.addFan(beundrer);
		idol.addFan(antiBeundrer);
		idol.setHårfarge("rød");
		System.out.println(idol.getHårfarge());
		System.out.println(beundrer.getHårfarge());
		System.out.println(antiBeundrer.getHårfarge());
		idol.setHårlengde(80);
		System.out.println(idol.getHårlengde());
		System.out.println(beundrer.getHårlengde());
		System.out.println(antiBeundrer.getHårlengde());
	}
}
