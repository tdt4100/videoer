package arv;

import java.util.Arrays;
import java.util.List;

/*
 * @startuml
 * class Object {
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * class Point extends Object {
 * 	double x
 * 	double y
 * 	String toString()
 * 	boolean equals(Object)
 * }
 * @enduml
 */
public class Point {

	private double x;
	private double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point[x=" + x + " y=" + y + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		// hvis o er et Point med samme x, y som this, så returneres true
		if (o instanceof Point) {
			Point other = (Point) o;
			return this.x == other.x && this.y == other.y;
		}
		return false;
	}

	public static void main(String[] args) {
		Object origo = new Point(0.0, 0.0);
		Point p1 = (Point) origo;
		Point p2 = new Point(0.0, 0.0);
		Point p3 = new Point(0.0, 0.1);
		//		System.out.println("Point[x=0.0 y=0.0]".equals(origo));
		List<Object> col = Arrays.asList(origo, p2);
		System.out.println(col.indexOf(p2));
	}
}
