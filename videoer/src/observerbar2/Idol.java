package observerbar2;

import java.util.ArrayList;
import java.util.Collection;

public class Idol {

	public final static String HAIR_COLOR_PROPERTY = "hairColor";
	public final static String HAIR_LENGTH_PROPERTY = "hairLength";

	private String hairColor = "black";
	private int hairLength = 40;

	private Collection<Fan> fans = new ArrayList<>();

	public void addFan(Fan fan) {
		this.fans.add(fan);
	}

	public void removeFan(Fan fan) {
		this.fans.remove(fan);
	}

	protected void firePropertyChanged(String property, Object oldValue, Object newValue) {
		for (Fan fan : fans) {
			fan.propertyChanged(this, property, oldValue, newValue);
		}
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		String oldHairColor = this.hairColor;
		this.hairColor = hairColor;
		firePropertyChanged(HAIR_COLOR_PROPERTY, oldHairColor, hairColor);
	}

	public int getHairLength() {
		return hairLength;
	}

	public void setHairLength(int hairLength) {
		int oldHairLength = this.hairLength;
		this.hairLength = hairLength;
		firePropertyChanged(HAIR_LENGTH_PROPERTY, oldHairLength, hairLength);
	}
}
