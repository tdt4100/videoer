package observerbar2;

public interface Fan {
	public void propertyChanged(Idol idol, String property, Object oldValue, Object newValue);
}
